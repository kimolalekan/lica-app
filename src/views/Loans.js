import React, { Component } from "react";
import {
  Text,
  ActivityIndicator,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  AsyncStorage
} from "react-native";
import { withNavigation } from "react-navigation";

import { Column as Col, Row } from "react-native-flexbox-grid";
import moment from "moment";
import currencyFormatter from "currency-formatter";

class Loans extends Component {
  constructor(props) {
    super(props);
    this.state = { loans: [] };
  }

  redirect = routeName => {
    const { navigate } = this.props.navigation;
    navigate(routeName);
  };

  formatMoney(val) {
    val = currencyFormatter.format(val, {
      symbol: "₦",
      decimal: ".",
      thousand: ",",
      precision: 0,
      format: "%s %v"
    });
    return val;
  }

  getLoans = async () => {
    let user = await AsyncStorage.getItem("user");

    let uri = `http://192.168.6.62:3050/api/loans/user?user=${user.id}`;
    fetch(uri, {
      headers: {
        apikey: "9a6301cf-f586-4989-99a6-a9d44a3ce915",
        "content-type": "application/json"
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data.error) {
          this.setState({ error: data.error, notify: "none" });
        } else {
          this.setState({ loans: data.data, notify: "none" });
        }
      })
      .catch(err => console.log(err));
  };

  componentDidMount() {
    this.getLoans();
  }

  render() {
    const { loans } = this.state;
    const loan = loans.map((item, key) => (
      <View key={key}>
        <Row>
          <Col sm={12} md={6} lg={6}>
            <TouchableOpacity style={styles.list}>
              <Row size={12}>
                <Col sm={10}>
                  <Text style={styles.title}>
                    {this.formatMoney(item.amount)}
                  </Text>
                  <Text style={styles.textStyle}>
                    {moment(item.createdAt).format("MMM D, Y h:mm A")}
                  </Text>
                </Col>
                <Col sm={2}>
                  <Text
                    style={{
                      fontSize: 10,
                      marginLeft: 10,
                      fontWeight: "bold"
                    }}
                  >
                    Status
                  </Text>
                  <Text style={{ fontSize: 10, marginLeft: 10, marginTop: 5 }}>
                    {item.status}
                  </Text>
                </Col>
              </Row>
            </TouchableOpacity>
          </Col>
        </Row>
      </View>
    ));

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <ScrollView>
          {/* <View style={{ marginTop: 10 }}>
          <ActivityIndicator color="#000" />
        </View> */}
          <View style={{ flex: 1, marginTop: 50 }}>
            {loan}
            <Row>
              <Col sm={12} md={6} lg={6}>
                <TouchableOpacity style={styles.list}>
                  <Row size={12}>
                    <Col sm={10}>
                      <Text style={styles.title}>{this.formatMoney(5000)}</Text>
                      <Text style={styles.textStyle}>
                        {moment().format("MMM D, Y h:mm A")}
                      </Text>
                    </Col>
                    <Col sm={2}>
                      <Text
                        style={{
                          fontSize: 10,
                          marginLeft: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Status
                      </Text>
                      <Text
                        style={{ fontSize: 10, marginLeft: 10, marginTop: 5 }}
                      >
                        pending
                      </Text>
                    </Col>
                  </Row>
                </TouchableOpacity>
              </Col>
            </Row>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f9f9f9",
    flex: 1
  },
  list: {
    backgroundColor: "#fff",
    padding: 7,
    borderTopColor: "#ccc",
    borderBottomColor: "transparent",
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderWidth: 1
  },

  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 3,
    marginLeft: 10
  },
  textStyle: {
    color: "#555",
    marginTop: 3,
    marginLeft: 10,
    fontSize: 12
  }
});
export default withNavigation(Loans);
