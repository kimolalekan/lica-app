import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  TextInput,
  ActivityIndicator,
  KeyboardAvoidingView,
  AsyncStorage
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";

export default class Forget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      error: "",
      notify: "none"
    };
  }
  static navigationOptions = {
    title: null,
    header: null
  };

  goHome = async data => {
    let code = `${data.code}`;
    await AsyncStorage.setItem("code", code);
    await AsyncStorage.setItem("activeUser", data.user);
    const { navigate } = this.props.navigation;
    navigate("Reset");
  };

  authenticate = () => {
    const { phone } = this.state;

    if (phone.length < 11) {
      this.setState({
        error: "Phone number is not valid!!!",
        notify: "none"
      });
    } else {
      this.setState({ notify: "flex" });
      let form = JSON.stringify({
        phone: `${phone}`
      });

      let uri = `http://192.168.6.62:3050/api/user/forgot`;
      fetch(uri, {
        method: "POST",
        headers: {
          apikey: "9a6301cf-f586-4989-99a6-a9d44a3ce915",
          "content-type": "application/json"
        },
        body: form
      })
        .then(res => res.json())
        .then(data => {
          if (data.error) {
            this.setState({ error: data.error, notify: "none" });
          } else {
            this.setState({ error: "", notify: "none" });
            this.goHome(data);
          }
        })
        .catch(err => console.log(err));
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    const { error, notify } = this.state;

    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />

        <ImageBackground
          style={styles.bg}
          source={require("../images/lica-bg.png")}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: "35%",
              bottom: 0
            }}
          >
            <View style={{ alignItems: "center" }}>
              <View
                style={{ width: 250, marginBottom: 20, alignItems: "center" }}
              >
                <View style={{ display: notify }}>
                  <Row>
                    <Col sm={3}>
                      <ActivityIndicator color="#003B55" size={"small"} />
                    </Col>
                    <Col sm={9}>
                      <Text style={{ color: "#003B55" }}>Processing....</Text>
                    </Col>
                  </Row>
                </View>

                <Text style={{ color: "#cb0000" }}>{error}</Text>
              </View>

              <TextInput
                placeholder="Phone number"
                keyboardType="phone-pad"
                maxLength={11}
                style={{
                  backgroundColor: "transparent",
                  paddingTop: 10,
                  paddingBottom: 10,
                  paddingLeft: 10,
                  paddingRight: 10,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 50,
                  width: 250,
                  fontSize: 18
                }}
                onChangeText={phone => this.setState({ phone })}
              />
              <TouchableOpacity
                onPress={() => this.authenticate()}
                style={{
                  backgroundColor: "#cb0000",
                  padding: 8,
                  margin: 5,
                  borderRadius: 5,
                  width: 250
                }}
              >
                <Row>
                  <Col sm={10}>
                    <Text
                      style={{
                        color: "#fff",
                        fontSize: 18
                      }}
                    >
                      Reset password
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Icon
                      name="ios-arrow-round-forward"
                      color="#fff"
                      size={30}
                    />
                  </Col>
                </Row>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
