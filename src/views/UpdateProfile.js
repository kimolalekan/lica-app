import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StatusBar,
  KeyboardAvoidingView,
  AsyncStorage,
  ActivityIndicator,
  TextInput
} from "react-native";

import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";
import Select from "../components/select";
import moment from "moment";
import DatePicker from "react-native-datepicker";

export default class UpdateProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
      password: "",
      birthDate: "",
      employmentStatus: "",
      account: "",
      accountNo: "",
      bvn: "",
      event: "",
      gender: "",
      bank: "",
      tab1color: "#000",
      tab2color: "#999",
      tab1label: 1,
      tab2label: 0,
      tab1: "flex",
      tab2: "none",
      error: "",
      notify: "none",
      activeId: "",
      text: ""
    };
  }

  static navigationOptions = {
    title: "Update Profile",
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" }
  };

  updateUser = async data => {
    data = JSON.stringify(data);
    await AsyncStorage.setItem("user", data);
  };

  getUser = async () => {
    let user = await AsyncStorage.getItem("user");
    user = JSON.parse(user);
    this.setState({ error: "", notify: "flex" });
    let uri = `http://192.168.43.200:3050/api/user/${user.id}`;

    fetch(uri, {
      headers: {
        apikey: "9a6301cf-f586-4989-99a6-a9d44a3ce915",
        "content-type": "application/json"
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data.error) {
          this.setState({ error: data.error, notify: "none" });
        } else {
          console.log(data);

          this.updateUser(data);
          this.setState({
            error: "",
            notify: "none",
            firstName: data.firstName,
            lastName: data.lastName,
            phone: data.phone,
            email: data.email,
            password: data.password,
            birthDate: data.birthDate,
            employmentStatus: data.employmentStatus,
            account: data.account,
            accountNo: data.accountNo,
            bvn: data.bvn,
            gender: data.gender,
            activeId: data.id
          });
        }
      })
      .catch(err => console.log(err));
  };

  handleTab = val => {
    if (val == 1) {
      this.setState({
        tab1color: "#000",
        tab2color: "#999",
        tab1label: 1,
        tab2label: 0,
        tab1: "flex",
        tab2: "none"
      });
    } else if (val == 2) {
      this.setState({
        tab1color: "#999",
        tab2color: "#000",
        tab1label: 0,
        tab2label: 1,
        tab1: "none",
        tab2: "flex"
      });
    }
  };

  handleDate = birthDate => {
    this.setState({ birthDate });
  };

  handleEmploy = (key, employmentStatus) => {
    this.setState({ employmentStatus });
  };

  handleGender = (key, gender) => {
    this.setState({ gender });
  };

  handleBank = (key, account) => {
    this.setState({ account });
  };

  validateEmail = email => {
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(email);
  };

  validatePersonal = () => {
    const { firstName, lastName, email, phone, birthDate, gender } = this.state;
    if (firstName.length < 2) {
      this.setState({ error: "First name is too short!" });
    } else if (lastName.length < 2) {
      this.setState({ error: "Last name is too short!" });
    } else if (!this.validateEmail(email)) {
      this.setState({ error: "Email is not valid!" });
    } else if (phone.length < 11) {
      this.setState({ error: "Phone number is not valid!" });
    } else if (birthDate.length < 1) {
      this.setState({ error: "Birth date is missing!" });
    } else if (gender.length < 4) {
      this.setState({ error: "Gender is missing!" });
    } else {
      this.setState({ error: "" });
      this.handleTab(2);
    }
  };

  validateStep = () => {
    let val = false;

    const { firstName, lastName, email, phone, birthDate, gender } = this.state;
    if (firstName.length < 2) {
      this.setState({ error: "First name is too short!" });
    } else if (lastName.length < 2) {
      this.setState({ error: "Last name is too short!" });
    } else if (!this.validateEmail(email)) {
      this.setState({ error: "Email is not valid!" });
    } else if (phone.length < 11) {
      this.setState({ error: "Phone number is not valid!" });
    } else if (birthDate.length < 1) {
      this.setState({ error: "Birth date is missing!" });
    } else if (gender.length < 4) {
      this.setState({ error: "Gender is missing!" });
    } else {
      this.setState({ error: "" });
      val = true;
    }

    return val;
  };

  sendData = data => {
    this.setState({ error: "", notify: "flex" });
    let uri = `http://192.168.6.62:3050/api/user/update`;
    fetch(uri, {
      method: "POST",
      headers: {
        apikey: "9a6301cf-f586-4989-99a6-a9d44a3ce915",
        "content-type": "application/json"
      },
      body: data
    })
      .then(res => res.json())
      .then(data => {
        if (data.error) {
          this.setState({ error: data.error, notify: "none" });
        } else {
          this.getUser();
          this.setState({
            error: "",
            notify: "none",
            text: "Profile updated!"
          });
        }
      })
      .catch(err => console.log(err));
  };

  processUser = () => {
    const {
      firstName,
      lastName,
      email,
      phone,
      birthDate,
      gender,
      employmentStatus,
      account,
      accountNo,
      bvn,
      activeId
    } = this.state;

    const form = JSON.stringify({
      firstName,
      lastName,
      email,
      phone,
      birthDate,
      gender,
      employmentStatus,
      account,
      accountNo,
      bvn,
      id: activeId
    });

    if (!this.validateStep()) {
    } else if (employmentStatus.length < 2) {
      this.setState({ error: "Employment status is missing!" });
    } else if (account.length < 2) {
      this.setState({ error: "Bank is required!" });
    } else if (accountNo.length < 10) {
      this.setState({ error: "Account number is not valid!" });
    } else if (bvn.length < 10) {
      this.setState({ error: "BVN is not valid!" });
    } else {
      this.setState({ error: "" });
      this.sendData(form);
    }
  };

  async componentDidMount() {
    this.getUser();
  }

  render() {
    const { navigate } = this.props.navigation;

    const {
      error,
      notify,
      tab1color,
      tab2color,
      tab1label,
      tab2label,
      tab1,
      tab2,
      gender,
      phone,
      employmentStatus,
      birthDate,
      firstName,
      lastName,
      email,
      text,
      bvn,
      accountNo,
      account
    } = this.state;
    let maxDate = moment().format("YYYY") - 21;
    maxDate = `${maxDate}-${moment().format("MM")}-${moment().format("DD")}`;

    const banks = [
      {
        label: "Access Bank"
      },
      {
        label: "Access Bank (Diamond)",
        value: "063"
      },
      {
        label: "ALAT by WEMA",
        value: "035"
      },
      {
        label: "ASO Savings and Loans",
        value: "501"
      },
      {
        label: "Citibank Nigeria",
        value: "023"
      },
      {
        label: "Ecobank Nigeria",
        value: "050"
      },
      {
        label: "Ekondo Microfinance Bank",
        value: "562"
      },
      {
        label: "Enterprise Bank",
        value: "084"
      },
      {
        label: "Fidelity Bank",
        value: "070"
      },
      {
        label: "First Bank of Nigeria",
        value: "011"
      },
      {
        label: "First City Monument Bank",
        value: "214"
      },
      {
        label: "Guaranty Trust Bank",
        value: "058"
      },
      {
        label: "Heritage Bank",
        value: "030"
      },
      {
        label: "Jaiz Bank",
        value: "301"
      },
      {
        label: "Keystone Bank",
        value: "082"
      },
      {
        label: "MainStreet Bank",
        value: "014"
      },
      {
        label: "Parallex Bank",
        value: "526"
      },
      {
        label: "Polaris Bank",
        value: "076"
      },
      {
        label: "Providus Bank",
        value: "101"
      },
      {
        label: "Stanbic IBTC Bank",
        value: "221"
      },
      {
        label: "Standard Chartered Bank",
        value: "068"
      },
      {
        label: "Sterling Bank",
        value: "232"
      },
      {
        label: "Suntrust Bank",
        value: "100"
      },
      {
        label: "Union Bank of Nigeria",
        value: "032"
      },
      {
        label: "United Bank For Africa",
        value: "033"
      },
      {
        label: "Unity Bank",
        value: "215"
      },
      {
        label: "Wema Bank",
        value: "035"
      },
      {
        label: "Zenith Bank",
        value: "057"
      }
    ];

    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />
        <ScrollView
          style={{
            flex: 1,
            width: "100%",
            marginBottom: 50
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center"
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: "center",
                padding: 30
              }}
            >
              <Row>
                <Col sm={6}>
                  <TouchableOpacity
                    style={{ borderBottomWidth: tab1label }}
                    onPress={() => this.handleTab(1)}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        color: tab1color
                      }}
                    >
                      Personal Information
                    </Text>
                  </TouchableOpacity>
                </Col>
                <Col sm={1}>
                  <Text>{""}</Text>
                </Col>
                <Col sm={5}>
                  <TouchableOpacity
                    style={{ borderBottomWidth: tab2label }}
                    onPress={() => this.handleTab(2)}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        color: tab2color
                      }}
                    >
                      Financial details
                    </Text>
                  </TouchableOpacity>
                </Col>
              </Row>
            </View>

            <View style={{ width: 250, marginBottom: 20 }}>
              <View style={{ display: notify }}>
                <ActivityIndicator size={"large"} />
              </View>
              <Text style={{ color: "#cb0000" }}>{error}</Text>
              <Text style={{ color: "#228b22" }}>{text}</Text>
            </View>

            <View style={{ display: tab1 }}>
              <TextInput
                placeholder="First name"
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 40,
                  width: 250,
                  fontSize: 14
                }}
                value={firstName}
                onChangeText={firstName => this.setState({ firstName })}
              />
              <TextInput
                placeholder="Last name"
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 40,
                  width: 250,
                  fontSize: 14
                }}
                value={lastName}
                onChangeText={lastName => this.setState({ lastName })}
              />
              <TextInput
                placeholder="Email"
                keyboardType="email-address"
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 40,
                  width: 250,
                  fontSize: 14
                }}
                value={email}
                onChangeText={email => this.setState({ email })}
              />
              <TextInput
                placeholder="Phone number"
                maxLength={11}
                keyboardType="phone-pad"
                value={phone}
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 40,
                  width: 250,
                  fontSize: 14
                }}
                value={phone}
                onChangeText={phone => this.setState({ phone })}
              />
              <DatePicker
                style={{
                  margin: 5,
                  borderWidth: 1,
                  borderRadius: 5,
                  borderColor: "#ccc",
                  width: 250,
                  height: 40,
                  paddingLeft: 10
                }}
                customStyles={{
                  dateInput: {
                    borderColor: "transparent",
                    alignItems: "flex-start",
                    fontSize: 14
                  }
                }}
                mode="date"
                placeholder="Date of birth"
                format="YYYY-MM-DD"
                showIcon={false}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                date={birthDate}
                maxDate={maxDate}
                onDateChange={date => this.handleDate(date)}
              />
              <Select
                data={[
                  { value: "Male", label: "Male" },
                  {
                    value: "Female",
                    label: "Female"
                  }
                ]}
                width={250}
                placeholder={"Gender"}
                value={gender}
                style={{
                  borderWidth: 1,
                  borderColor: "#CCC",
                  borderRadius: 5,
                  marginLeft: 5,
                  marginTop: 5,
                  marginBottom: 5
                }}
                onSelect={this.handleGender.bind(this)}
                search={true}
              />
              <TouchableOpacity
                onPress={() => this.validatePersonal()}
                style={{
                  backgroundColor: "#cb0000",
                  paddingTop: 7,
                  paddingBottom: 7,
                  paddingLeft: 5,
                  paddingRight: 5,
                  margin: 5,
                  marginLeft: 5,
                  borderRadius: 5,
                  width: 250,
                  fontSize: 14
                }}
              >
                <Row>
                  <Col sm={10}>
                    <Text
                      style={{
                        color: "#fff",

                        fontSize: 16
                      }}
                    >
                      Continue
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Icon
                      name="ios-arrow-round-forward"
                      color="#fff"
                      size={23}
                    />
                  </Col>
                </Row>
              </TouchableOpacity>
            </View>
            <View style={{ display: tab2 }}>
              <Select
                value={employmentStatus}
                data={[
                  { value: "Employed", label: "Employed" },
                  {
                    value: "Self employed",
                    label: "Self employed"
                  },
                  {
                    value: "Unemployed",
                    label: "Unemployed"
                  }
                ]}
                width={250}
                placeholder={"Employment status"}
                style={{
                  borderWidth: 1,
                  borderColor: "#CCC",
                  borderRadius: 5,
                  marginTop: 5,
                  marginBottom: 5
                }}
                onSelect={this.handleEmploy.bind(this)}
                search={true}
              />
              <Select
                value={account}
                data={banks}
                width={250}
                placeholder={"Bank name"}
                style={{
                  borderWidth: 1,
                  borderColor: "#CCC",
                  borderRadius: 5,
                  marginTop: 5,
                  marginBottom: 5
                }}
                onSelect={this.handleBank.bind(this)}
                search={true}
              />
              <TextInput
                placeholder="  Account number"
                keyboardType="phone-pad"
                maxLength={10}
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  marginLeft: 0,
                  marginRight: 0,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 40,
                  width: 250,
                  fontSize: 14
                }}
                value={accountNo}
                onChangeText={accountNo => this.setState({ accountNo })}
              />
              <TextInput
                placeholder="  BVN number"
                maxLength={10}
                keyboardType="phone-pad"
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  marginLeft: 0,
                  marginRight: 0,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 40,
                  width: 250,
                  fontSize: 14
                }}
                value={bvn}
                onChangeText={bvn => this.setState({ bvn })}
              />

              <TouchableOpacity
                onPress={() => this.processUser()}
                style={{
                  backgroundColor: "#cb0000",
                  paddingTop: 7,
                  paddingBottom: 7,
                  paddingLeft: 5,
                  paddingRight: 5,
                  margin: 5,
                  marginLeft: 0,
                  borderRadius: 5,
                  width: 250
                }}
              >
                <Row>
                  <Col sm={10}>
                    <Text
                      style={{
                        color: "#fff",

                        fontSize: 16
                      }}
                    >
                      Update
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Icon
                      name="ios-arrow-round-forward"
                      color="#fff"
                      size={23}
                    />
                  </Col>
                </Row>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
