import React, { Component } from "react";
import {
  Text,
  ActivityIndicator,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  AsyncStorage
} from "react-native";
import { withNavigation } from "react-navigation";

import { Column as Col, Row } from "react-native-flexbox-grid";
import moment from "moment";

class Loan extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = {
    title: null,
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" }
  };

  redirect = routeName => {
    const { navigate } = this.props.navigation;
    navigate(routeName);
  };

  shortify(text) {
    const length = 67;
    if (text.length > length) {
      text = `${text.substring(0, length)}`;
      text = `${text.trim()}...`;
    }
    return text;
  }

  async setStatus() {
    await AsyncStorage.setItem("status", "Chats");
  }

  componentDidMount() {
    this.setStatus();
  }

  render = () => (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <ScrollView>
        {/* <View style={{ marginTop: 10 }}>
          <ActivityIndicator color="#000" />
        </View> */}
        <View style={{ flex: 1, marginTop: 50 }}>
          <Text
            style={{
              textAlign: "center",
              fontSize: 16,
              fontWeight: "bold",
              marginBottom: 20
            }}
          >
            Tenure: 1 month
          </Text>
          <Row>
            <Col sm={12} md={6} lg={6}>
              <TouchableOpacity style={styles.list}>
                <Row size={12}>
                  <Col sm={10}>
                    <Text style={styles.title}>&#8358; 5,000</Text>
                    <Text style={styles.textStyle}>
                      {moment().format("MMM D, Y h:mm A")}
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <View style={{ marginTop: 10 }}>
                      <Text
                        style={{
                          fontSize: 10,
                          marginLeft: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Loan
                      </Text>
                      <Text
                        style={{ fontSize: 10, marginLeft: 10, marginTop: 5 }}
                      >
                        Disbursed
                      </Text>
                    </View>
                  </Col>
                </Row>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row>
            <Col sm={12} md={6} lg={6}>
              <TouchableOpacity style={styles.list}>
                <Row size={12}>
                  <Col sm={10}>
                    <Text style={styles.title}>&#8358; 5,000 (principal)</Text>
                    <Text style={styles.textStyle}>
                      {moment().format("MMM D, Y h:mm A")}
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Text
                      style={{
                        fontSize: 10,
                        marginLeft: 10,
                        fontWeight: "bold"
                      }}
                    >
                      Status
                    </Text>
                    <Text
                      style={{ fontSize: 10, marginLeft: 10, marginTop: 5 }}
                    >
                      Paid
                    </Text>
                  </Col>
                </Row>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row>
            <Col sm={12} md={6} lg={6}>
              <TouchableOpacity style={styles.list}>
                <Row size={12}>
                  <Col sm={10}>
                    <Text style={styles.title}>&#8358; 2,500 (interest)</Text>
                    <Text style={styles.textStyle}>
                      {moment().format("MMM D, Y h:mm A")}
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Text
                      style={{
                        fontSize: 10,
                        marginLeft: 10,
                        fontWeight: "bold"
                      }}
                    >
                      Status
                    </Text>
                    <Text
                      style={{ fontSize: 10, marginLeft: 10, marginTop: 5 }}
                    >
                      Resolved
                    </Text>
                  </Col>
                </Row>
              </TouchableOpacity>
            </Col>
          </Row>
        </View>
      </ScrollView>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f9f9f9",
    flex: 1
  },
  list: {
    backgroundColor: "#fff",
    padding: 7,
    borderTopColor: "#ccc",
    borderBottomColor: "transparent",
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderWidth: 1
  },

  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 3,
    marginLeft: 10
  },
  title2: {
    fontSize: 30,
    fontWeight: "bold"
  },
  textStyle: {
    color: "#555",
    marginTop: 3,
    marginLeft: 10,
    fontSize: 12
  }
});
export default withNavigation(Loan);
