import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  AsyncStorage
} from "react-native";

import { Column as Col, Row } from "react-native-flexbox-grid";
import currencyFormatter from "currency-formatter";
import Modal from "react-native-modal";
import Tc from "../components/Tc";

export default class LoanOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: "",
      isModalVisible: false,
      success: false,
      bg1: "transparent",
      bg2: "transparent",
      bg3: "transparent",
      bg4: "transparent",
      txt1: "#003B55",
      txt2: "#003B55",
      txt3: "#003B55",
      txt4: "#003B55",
      error: "",
      notify: ""
    };
  }

  static navigationOptions = {
    title: "Loan Offers",
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" }
  };

  formatMoney(val) {
    val = currencyFormatter.format(val, {
      symbol: "₦",
      decimal: ".",
      thousand: ",",
      precision: 0,
      format: "%s %v"
    });
    return val;
  }

  toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    });
  };

  toggleNotify = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    });
    setTimeout(() => {
      this.setState({
        success: true
      });
    }, 500);
  };

  handleButton = (val, amount) => {
    this.setState({ amount });
    if (val == 0) {
      this.setState({
        bg1: "#003B55",
        bg2: "transparent",
        bg3: "transparent",
        bg4: "transparent",
        txt1: "#fff",
        txt2: "#003B55",
        txt3: "#003B55",
        txt4: "#003B55"
      });
    } else if (val == 1) {
      this.setState({
        bg2: "#003B55",
        bg1: "transparent",
        bg3: "transparent",
        bg4: "transparent",
        txt2: "#fff",
        txt1: "#003B55",
        txt3: "#003B55",
        txt4: "#003B55"
      });
    } else if (val == 2) {
      this.setState({
        bg3: "#003B55",
        bg1: "transparent",
        bg2: "transparent",
        bg4: "transparent",
        txt3: "#fff",
        txt1: "#003B55",
        txt2: "#003B55",
        txt4: "#003B55"
      });
    } else if (val == 3) {
      this.setState({
        bg4: "#003B55",
        bg1: "transparent",
        bg3: "transparent",
        bg2: "transparent",
        txt4: "#fff",
        txt2: "#003B55",
        txt3: "#003B55",
        txt1: "#003B55"
      });
    }
  };

  bg = val => {
    const { bg1, bg2, bg3, bg4 } = this.state;
    let res;
    if (val == 0) {
      res = bg1;
    } else if (val == 1) {
      res = bg2;
    } else if (val == 2) {
      res = bg3;
    } else if (val == 3) {
      res = bg4;
    }

    return res;
  };

  txt = val => {
    const { txt1, txt2, txt3, txt4 } = this.state;
    let res;
    if (val == 0) {
      res = txt1;
    } else if (val == 1) {
      res = txt2;
    } else if (val == 2) {
      res = txt3;
    } else if (val == 3) {
      res = txt4;
    }

    return res;
  };

  currentLoan = async loans => {
    await AsyncStorage.setItem("loan", loans);
  };

  goHome = () => {
    this.setState({
      success: false
    });
    const { navigate } = this.props.navigation;
    navigate("Main");
  };

  proceed = async () => {
    let user = await AsyncStorage.getItem("user");
    user = JSON.parse(user);

    const { amount } = this.state;

    let form = JSON.stringify({
      user: user.id,
      amount: amount,
      duration: "30",
      narration: "Loan request"
    });

    if (amount) {
      let uri = `http://192.168.43.200:3050/api/loan/request`;
      fetch(uri, {
        method: "POST",
        headers: {
          apikey: "9a6301cf-f586-4989-99a6-a9d44a3ce915",
          "content-type": "application/json"
        },
        body: form
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);

          if (data.error) {
            this.setState({ error: data.error, notify: "none" });
          } else {
            data = JSON.stringify(data);
            this.currentLoan(data);
            this.toggleNotify();
          }
        })
        .catch(err => console.log(err));
    }
  };

  interest = val => {
    let res;
    res = val * 0.3;
    res = val + res;
    return res;
  };

  render() {
    const { txt1, txt2, txt3, txt4, bg1, bg2, bg3, bg4, amount } = this.state;

    let loan = [
      { amount: 5000, repayment: this.interest(5000) },
      { amount: 4000, repayment: this.interest(4000) },
      { amount: 3000, repayment: this.interest(3000) }
    ];

    const loans = loan.map((item, key) => (
      <View
        key={key}
        style={{
          marginBottom: 10,
          paddingBottom: 10,
          borderBottomColor: "#ccc",
          borderBottomWidth: 1
        }}
      >
        <Row>
          <Col sm={4}>
            <TouchableOpacity
              style={[styles.selected, { backgroundColor: this.bg(key) }]}
              onPress={() => this.handleButton(key, item.amount)}
            >
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "bold",
                  color: this.txt(key)
                }}
              >
                {this.formatMoney(item.amount)}
              </Text>
            </TouchableOpacity>
          </Col>
          <Col sm={2}>
            <Text>{""}</Text>
          </Col>
          <Col sm={6}>
            <TouchableOpacity
              style={[styles.none, { backgroundColor: "transparent" }]}
            >
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "bold"
                }}
              >
                Pay &nbsp; {this.formatMoney(item.repayment)}
              </Text>
            </TouchableOpacity>
          </Col>
        </Row>
      </View>
    ));

    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 0.9 }}>
          <StatusBar barStyle="light-content" />
          <Modal isVisible={this.state.success}>
            <View
              style={{
                backgroundColor: "#fff",
                padding: 20
              }}
            >
              <Text style={{ fontSize: 20, fontweight: "bold" }}>
                Your loan is being processed. We will notify via SMS when the
                loan is disbursed.
              </Text>

              <TouchableOpacity
                style={{
                  backgroundColor: "#228b22",
                  padding: 10,
                  alignSelf: "flex-end",
                  marginTop: 20,
                  borderRadius: 3
                }}
                onPress={() => this.goHome()}
              >
                <Text style={{ color: "#fff", fontSize: 16 }}>Okay</Text>
              </TouchableOpacity>
            </View>
          </Modal>
          <Modal isVisible={this.state.isModalVisible}>
            <View
              style={{
                height: 400,
                backgroundColor: "#fff",
                padding: 20
              }}
            >
              <View style={{ flex: 0.8 }}>
                <ScrollView>
                  <Tc />
                </ScrollView>
              </View>
              <View style={{ flex: 0.2 }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: "#228b22",
                    padding: 10,
                    alignSelf: "flex-end",
                    marginTop: 10,
                    borderRadius: 3
                  }}
                  onPress={() => this.proceed()}
                >
                  <Text style={{ color: "#fff", fontSize: 16 }}>I Agree</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <View
            style={{
              margin: 10,
              marginTop: 30,
              padding: 30,
              alignSelf: "center"
            }}
          >
            {loans}
          </View>
        </ScrollView>
        <View style={{ flex: 0.1 }}>
          <TouchableOpacity
            onPress={() => this.toggleModal()}
            style={{
              backgroundColor: "#228b22",
              padding: 20,
              marginTop: -50,
              marginLeft: 5,
              marginRight: 5,
              borderRadius: 5
            }}
          >
            <Text
              style={{
                color: "#fff",
                textAlign: "center",
                fontSize: 20
              }}
            >
              Get Loan
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  loader: {
    marginTop: 100
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontSize: 25,
    fontWeight: "bold"
  },
  selected: {
    padding: 10,
    borderWidth: 2,
    borderColor: "#003B55",
    borderRadius: 5
  },
  none: {
    padding: 10,
    borderWidth: 2,
    borderColor: "transparent",
    borderRadius: 5
  }
});
