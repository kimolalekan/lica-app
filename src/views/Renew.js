import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  AsyncStorage,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";

export default class Renew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pass: "",
      verify: "",
      error: "",
      notify: "none"
    };
  }
  static navigationOptions = {
    title: null,
    header: null
  };

  goHome = async () => {
    await AsyncStorage.clear();
    const { navigate } = this.props.navigation;
    navigate("Login");
  };

  validate = async () => {
    let user = await AsyncStorage.getItem("activeUser");
    let { verify, pass } = this.state;
    if (pass.length < 4) {
      this.setState({ error: "Type pin!!!" });
    } else if (verify.length < 4) {
      this.setState({ error: "Retype pin!!!" });
    } else if (verify !== pass) {
      this.setState({ error: "Pin does not match!!!" });
    } else {
      this.setState({ notify: "flex" });
      let form = JSON.stringify({ id: user, password: pass });
      let uri = `http://192.168.6.62:3050/api/user/update`;

      fetch(uri, {
        method: "POST",
        headers: {
          apikey: "9a6301cf-f586-4989-99a6-a9d44a3ce915",
          "content-type": "application/json"
        },
        body: form
      })
        .then(res => res.json())
        .then(data => {
          if (!data.success) {
            this.setState({ error: "Error! Try again", notify: "none" });
          } else {
            this.setState({ error: "", notify: "none" });
            this.goHome();
          }
        })
        .catch(err => console.log(err));
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    const { error, notify } = this.state;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />

        <ImageBackground
          style={styles.bg}
          source={require("../images/lica-bg.png")}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: "35%",
              bottom: 0
            }}
          >
            <View style={{ alignItems: "center" }}>
              <View
                style={{
                  width: 250,
                  marginBottom: 20,
                  alignItems: "center"
                }}
              >
                <View style={{ display: notify }}>
                  <Row>
                    <Col sm={3}>
                      <ActivityIndicator color="#003B55" size={"small"} />
                    </Col>
                    <Col sm={9}>
                      <Text style={{ color: "#003B55" }}>Processing....</Text>
                    </Col>
                  </Row>
                </View>

                <Text style={{ color: "#cb0000" }}>{error}</Text>
              </View>

              <TextInput
                placeholder="Pin"
                keyboardType="phone-pad"
                minLength={4}
                maxLength={4}
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 50,
                  width: 250,
                  fontSize: 20
                }}
                onChangeText={pass => this.setState({ pass })}
              />
              <TextInput
                placeholder="Retype pin"
                keyboardType="phone-pad"
                minLength={4}
                maxLength={4}
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 50,
                  width: 250,
                  fontSize: 20
                }}
                onChangeText={verify => this.setState({ verify })}
              />
              <TouchableOpacity
                onPress={() => this.validate()}
                style={{
                  backgroundColor: "#cb0000",
                  padding: 8,
                  margin: 5,
                  borderRadius: 5,
                  width: 250
                }}
              >
                <Row>
                  <Col sm={10}>
                    <Text
                      style={{
                        color: "#fff",
                        fontSize: 18
                      }}
                    >
                      Update password
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Icon
                      name="ios-arrow-round-forward"
                      color="#fff"
                      size={30}
                    />
                  </Col>
                </Row>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
