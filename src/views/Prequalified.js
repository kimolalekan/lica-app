import React from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";

export default class Prequalified extends React.Component {
  state = {
    amount: ""
  };

  static navigationOptions = {
    title: "Loan request...",
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" },
    headerBack: null
  };

  componentDidMount() {
    const { navigate } = this.props.navigation;
    setTimeout(() => {
      navigate("LoanOption");
    }, 5000);
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />

        <View
          style={{
            alignItems: "center",
            marginTop: 100
          }}
        >
          <ActivityIndicator color="#003B55" size={"large"} />

          <Text
            style={{
              fontSize: 18,
              fontWeight: "bold",
              textAlign: "center",
              marginTop: 20
            }}
          >
            Processing your loan request....
          </Text>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
