import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  KeyboardAvoidingView,
  AsyncStorage,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";

export default class Verify extends React.Component {
  constructor(props) {
    super(props);
    this.state = { verify: "", error: "" };
  }
  static navigationOptions = {
    title: null,
    header: null
  };

  goHome = async () => {
    const { navigate } = this.props.navigation;
    navigate("Signup");
  };

  validate = async () => {
    let verify = this.state.verify;
    let code = await AsyncStorage.getItem("code");
    console.log("Processing...");

    if (code === verify) {
      this.setState({ error: "" });
      console.log("True");
      const { navigate } = this.props.navigation;
      navigate("Signup");
    } else {
      this.setState({ error: "Invalid code!!!" });
      console.log("False");
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />

        <ImageBackground
          style={styles.bg}
          source={require("../images/lica-bg.png")}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: "40%",
              bottom: 0
            }}
          >
            <View style={{ alignItems: "center" }}>
              <Text style={{ marginBottom: 10 }}>
                Enter the code sent to your phone.
              </Text>
              <Text style={{ marginBottom: 20 }}>{this.state.error}</Text>
              <TextInput
                placeholder="Enter Code"
                keyboardType="phone-pad"
                maxLength={6}
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 50,
                  fontSize: 18,
                  width: 250
                }}
                onChangeText={verify => this.setState({ verify })}
              />
              <TouchableOpacity
                onPress={() => this.validate()}
                style={{
                  backgroundColor: "#cb0000",
                  padding: 5,
                  margin: 5,
                  borderRadius: 5,
                  width: 250
                }}
              >
                <Row>
                  <Col sm={10}>
                    <Text
                      style={{
                        color: "#fff",
                        fontSize: 16
                      }}
                    >
                      Continue
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Icon
                      name="ios-arrow-round-forward"
                      color="#fff"
                      size={28}
                    />
                  </Col>
                </Row>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate("SmsVerify")}>
                <Text style={{ marginTop: 10, color: "#003B55" }}>
                  Didn't get code? Click here
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
