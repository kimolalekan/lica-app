import React, { Component } from "react";
import ReactNative, {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  StatusBar,
  AsyncStorage
} from "react-native";
import { withNavigation } from "react-navigation";
import * as Progress from "react-native-progress";
import { Column as Col, Row } from "react-native-flexbox-grid";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "" };
  }

  redirect = routeName => {
    const { navigate } = this.props.navigation;
    navigate(routeName);
  };

  async componentDidMount() {
    let user = await AsyncStorage.getItem("user");

    user = JSON.parse(user);
    this.setState({ name: `${user.firstName}` });
  }

  render() {
    const { name } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: "#f9f9f9" }}>
        <StatusBar barStyle="light-content" />

        <ScrollView style>
          <View style={{ marginTop: 20, alignItems: "center", padding: 20 }}>
            <Text style={{ fontSize: 20, fontWeight: "bold", margin: 10 }}>
              Hello, {name}
            </Text>
          </View>
          {/* <View
            style={{
              padding: 10,
              alignItems: "center",
              marginTop: 50
            }}
          >
            <Text
              style={{ marginBottom: 10, fontSize: 16, fontWeight: "bold" }}
            >
              Credit Score
            </Text>
            <Progress.Bar
              progress={0.3}
              width={200}
              height={20}
              color={"#228b22"}
            />
          </View>
           */}
          <View
            style={{
              marginTop: 40,
              padding: 10,
              marginLeft: 30,
              marginRight: 30
            }}
          >
            <Row>
              <Col sm={6}>
                <TouchableOpacity
                  onPress={() => this.redirect("Terms")}
                  style={{
                    backgroundColor: "#003B55",
                    padding: 10,
                    borderRadius: 3,
                    marginRight: 7
                  }}
                >
                  <Text
                    style={{ color: "#fff", textAlign: "center", fontSize: 14 }}
                  >
                    Loan Terms
                  </Text>
                </TouchableOpacity>
              </Col>
              <Col sm={6}>
                <TouchableOpacity
                  onPress={() => this.redirect("Privacy")}
                  style={{
                    backgroundColor: "#003B55",
                    padding: 10,
                    borderRadius: 3
                  }}
                >
                  <Text
                    style={{ color: "#fff", textAlign: "center", fontSize: 14 }}
                  >
                    Privacy Policy
                  </Text>
                </TouchableOpacity>
              </Col>
            </Row>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default withNavigation(Profile);
