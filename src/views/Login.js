import React from "react";
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  StatusBar,
  Image,
  ActivityIndicator,
  TextInput,
  AsyncStorage
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      password: "",
      error: "",
      notify: "none"
    };
  }
  static navigationOptions = {
    title: null,
    header: null
  };

  goHome = async user => {
    user = JSON.stringify(user);
    await AsyncStorage.setItem("user", user);
    const { navigate } = this.props.navigation;
    navigate("Main");
  };

  authenticate = () => {
    const { phone, password } = this.state;

    if (phone.length < 11) {
      this.setState({ error: "Phone number is not valid!!!", notify: "none" });
    } else if (password.length < 4) {
      this.setState({ error: "Pin is not valid!!!", notify: "none" });
    } else {
      this.setState({ notify: "flex" });
      let form = JSON.stringify({
        phone: `${phone}`,
        password: `${password}`
      });

      let uri = `http://192.168.6.62:3050/api/user/login`;
      fetch(uri, {
        method: "POST",
        headers: {
          apikey: "9a6301cf-f586-4989-99a6-a9d44a3ce915",
          "content-type": "application/json"
        },
        body: form
      })
        .then(res => res.json())
        .then(data => {
          if (data.error) {
            this.setState({ error: data.error, notify: "none" });
          } else {
            this.setState({ error: "", notify: "none" });
            this.goHome(data);
          }
        })
        .catch(err => console.log(err));
    }
  };

  render() {
    const { error, notify } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />

        <View style={{ alignItems: "center" }}>
          <Image
            source={require("../images/lica-logo.png")}
            style={{
              height: 100,
              resizeMode: "contain",
              marginTop: 30,
              marginBottom: 50
            }}
          />

          <View style={{ width: 250, marginBottom: 20, alignItems: "center" }}>
            <View style={{ display: notify }}>
              <Row>
                <Col sm={3}>
                  <ActivityIndicator color="#003B55" size={"small"} />
                </Col>
                <Col sm={9}>
                  <Text style={{ color: "#003B55" }}>Processing....</Text>
                </Col>
              </Row>
            </View>

            <Text style={{ color: "#cb0000" }}>{error}</Text>
          </View>

          <TextInput
            placeholder="Phone number"
            keyboardType="phone-pad"
            maxLength={11}
            style={{
              backgroundColor: "transparent",
              padding: 7,
              margin: 5,
              borderColor: "#ccc",
              borderWidth: 1,
              borderRadius: 5,
              height: 50,
              width: 250,
              fontSize: 18
            }}
            onChangeText={phone => this.setState({ phone })}
          />
          <TextInput
            placeholder="Pin"
            keyboardType="phone-pad"
            minLength={4}
            maxLength={4}
            secureTextEntry={true}
            style={{
              backgroundColor: "transparent",
              padding: 7,
              margin: 5,
              borderColor: "#ccc",
              borderWidth: 1,
              borderRadius: 5,
              height: 50,
              fontSize: 18,
              width: 250
            }}
            onChangeText={password => this.setState({ password })}
          />
          <TouchableOpacity
            onPress={() => this.authenticate()}
            style={{
              backgroundColor: "#cb0000",
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 10,
              paddingRight: 5,
              margin: 5,
              borderRadius: 5,
              width: 250,
              fontSize: 16
            }}
          >
            <Row>
              <Col sm={10}>
                <Text
                  style={{
                    color: "#fff",
                    fontSize: 20
                  }}
                >
                  Login
                </Text>
              </Col>
              <Col sm={2}>
                <Icon name="ios-arrow-round-forward" color="#fff" size={30} />
              </Col>
            </Row>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigate("Forget")}>
            <Text style={{ marginTop: 12, fontSize: 14, color: "#003B55" }}>
              Forget password? Click here to reset.
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigate("SmsVerify")}>
            <Text style={{ marginTop: 12, fontSize: 14, color: "#003B55" }}>
              Don't have account? Click here to register.
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
