import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  KeyboardAvoidingView,
  TextInput,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";

export default class Landing extends React.Component {
  constructor(props) {
    super(props);
    this.state = { phone: "", error: "", animate: false, show: "none" };
  }
  static navigationOptions = {
    title: null,
    header: null
  };

  validate = () => {
    const phone = this.state.phone;
    let res = phone.length == 11 ? true : false;
    return res;
  };

  setCode = async val => {
    val = `${val}`;

    await AsyncStorage.setItem("code", val);
    const phone = this.state.phone;
    await AsyncStorage.setItem("phone", phone);
    this.props.navigation.navigate("Verify");
  };

  verify = () => {
    let validate = this.validate();

    if (validate) {
      this.setState({ animate: true, error: "" });

      let uri = `http://192.168.6.62:3050/api/verify/new`;
      let body = JSON.stringify({ phone: this.state.phone });
      fetch(uri, {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: body
      })
        .then(res => res.json())
        .then(val => {
          if (val.status === false) {
            this.setCode(val.code);
            this.setState({ animate: false, error: "", show: "none" });
          } else {
            this.setState({
              animate: false,
              error: "Phone number is registered on Lica!",
              show: "flex"
            });
          }
        })
        .catch(err => console.log(err));
    } else {
      this.setState({
        error: "Please enter a valid phone number!!!",
        animate: false
      });
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    const { error, animate, show } = this.state;
    return (
      <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
        <StatusBar barStyle="light-content" />

        <ImageBackground
          style={styles.bg}
          source={require("../images/lica-bg.png")}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: "30%",
              bottom: 0
            }}
          >
            <View style={{ alignItems: "center" }}>
              <ActivityIndicator
                size={"large"}
                color="#003B55"
                animating={animate}
              />
              <Text style={{ color: "#cb0000", marginBottom: 10 }}>
                {error}
              </Text>
              <View style={{ width: 250, marginBottom: 20, display: show }}>
                <Row>
                  <Col sm={6}>
                    <TouchableOpacity
                      style={{
                        borderWidth: 1,
                        borderColor: "#003B55",
                        borderRadius: 3,
                        padding: 5,
                        marginLeft: 5,
                        marginRight: 5
                      }}
                      onPress={() => navigate("Login")}
                    >
                      <Text
                        style={{
                          fontSize: 16,
                          textAlign: "center",
                          color: "#003B55"
                        }}
                      >
                        Sign in
                      </Text>
                    </TouchableOpacity>
                  </Col>
                  <Col sm={6}>
                    <TouchableOpacity
                      style={{
                        borderWidth: 1,
                        borderColor: "#003B55",
                        borderRadius: 3,
                        padding: 5,
                        marginLeft: 5,
                        marginRight: 5
                      }}
                      onPress={() => navigate("Forget")}
                    >
                      <Text
                        style={{
                          fontSize: 16,
                          textAlign: "center",
                          color: "#003B55"
                        }}
                      >
                        Reset pin
                      </Text>
                    </TouchableOpacity>
                  </Col>
                </Row>
              </View>
              <TextInput
                placeholder="Phone number"
                keyboardType="phone-pad"
                maxLength={11}
                style={{
                  backgroundColor: "transparent",
                  padding: 10,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 50,
                  width: 250,
                  fontSize: 18
                }}
                onChangeText={phone => this.setState({ phone })}
              />
              <TouchableOpacity
                onPress={() => this.verify()}
                style={{
                  backgroundColor: "#cb0000",
                  padding: 8,
                  margin: 5,
                  borderRadius: 5,
                  width: 250
                }}
              >
                <Row>
                  <Col sm={10}>
                    <Text
                      style={{
                        color: "#fff",

                        fontSize: 16
                      }}
                    >
                      Continue
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Icon
                      name="ios-arrow-round-forward"
                      color="#fff"
                      size={28}
                    />
                  </Col>
                </Row>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
