import React from "react";
import { StyleSheet, Text, View, ScrollView, StatusBar } from "react-native";

export default class Privacy extends React.Component {
  static navigationOptions = {
    title: "Privacy Policy",
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" }
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <StatusBar barStyle="light-content" />

        <View style={{ margin: 10 }}>
          <Text style={{ marginBottom: 15 }}>
            Lica is an online lending platform operated by Modellica Partners
            Ltd, and we are committed to protecting and respecting your privacy.
            This privacy policy regulates how personal information you provide
            on Lica App or website or we collect from you can be used or
            processed by us, our subsidiaries, affiliates, officers, employees
            or agents. Please read the following carefully to understand our
            views and practices regarding your personal data and how we will
            treat it.
          </Text>
          <Text style={{ marginBottom: 15 }}>
            By downloading and signing up on Lica App, you confirm that you have
            read and understood the terms and practices described in this
            policy. You accept and consent to gathering, usage, storing,
            processing and disclosure of your personal data in the manner set
            out in this policy. Also, you consent to collecting information from
            relevant third parties as may be necessary in respect of the
            services extended to you via Lica App.
          </Text>
          <Text style={{ marginBottom: 15 }}>
            The collection and processing of your personal data is in accordance
            with the National Information Technology Development Agency Act 2007
            (the Act) and the provisions and prescriptions of Section 5; Part 1
            and Part 2 of National Information Systems and Network Security
            Standards and Guidelines.
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Information we may collect from you
          </Text>
          <Text style={{ marginBottom: 15 }}>
            We may collect and process the following data about you.
          </Text>
          <Text style={{ marginBottom: 15, fontWeight: "bold" }}>
            Information you give while using Lica App
          </Text>
          <Text style={{ marginBottom: 15 }}>
            This may include information; provided while creating your Account
            on the Lica App or on our website provided by e-mail or chat,
            telephone correspondence with us provided when, applying for a loan,
            searching for Lica App features, transactional information based on
            your activities on the App, the applications you use on social media
            sites, and when you report a problem encountered while using Lica
            App, its services, or our website Such as your name, e-mail address
            and phone number, age, username, password and other registration
            information, financial information (bank account details, debit card
            details and bank verification number), employment details, device's
            phone number
          </Text>
          <Text style={{ marginBottom: 15, fontWeight: "bold" }}>
            Information we collect about you and your device.
          </Text>
          <Text style={{ marginBottom: 15 }}>
            Every time you use Lica App, we may automatically collect the
            following information; Technical information, including but not
            limited to device ID & type, unique device identifiers (e.g. your
            device's IMEI or serial number), IP address of your device, your
            mobile operating system, information about the SIM card used by the
            Device, mobile network information, your Device’s operating system,
            your browser type, or your device’s location and time zone setting
            (Device Information) and other diagnostic data; Information stored
            on your device, including contact lists, call logs, SMS logs,
            activities and contact lists on other social media accounts, other
            Apps running on your Device GPS technology to gather geo-location
            information. However, your consent to collection, processing or use
            of this information can be withdrawn at any time by simply
            uninstalling the Lica App on your Device
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Information we receive from other sources (Third Party Information)
          </Text>
          <Text style={{ marginBottom: 15 }}>
            The services we provide requires that we partner with some third
            parties (payment processors, credit bureaus, financial services and
            credit providers, data analytics companies) and we may receive
            information about you from them.
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Tracking and cookies
          </Text>
          <Text style={{ marginBottom: 15 }}>
            We may use mobile tracking technologies and/or cookies to
            distinguish you from other users of the App or website. This helps
            us to improve your experience while using the App or when you browse
            our website. Also, it helps us to improve the App and website.
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            What we use your information for
          </Text>
          <Text style={{ marginBottom: 15 }}>
            We may use and retain your information for any or all of the
            following purposes: Process your loan application and determine your
            suitability Credit assessment, conduct credit checks and amount of
            such loan Terms and conditions applicable to the loan To improve our
            products and services offerings Internal operational purposes
            (market research, data analysis, surveys) targeted at improving our
            products and services For promotional campaigns and target marketing
            to you and other users of Lica products and services Notify you of
            new product offerings, changes to our services and our platforms To
            create a credit score which may be shared with third parties for
            cross selling purposes Identity verification, government sanctions
            screening and due diligence checks For debt recovery purposes (we
            may submit your information to third party collection companies, and
            credit bureaus where necessary) To comply with an order of the
            Court, Arbitration Panel, Tribunal, Regulatory Directive or any
            other legal or regulatory obligation To seek professional advice, in
            connection with any processes, claims or proceedings instituted by
            or against us as a consequence of any product or service provided to
            you
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Disclosure of your information
          </Text>
          <Text style={{ marginBottom: 15 }}>
            We may share your personal information with any of our subsidiaries,
            and with selected third parties: Including business partners,
            suppliers and sub-contractors for the performance of any contract we
            enter into with them or you. In the event that we sell or buy any
            business or assets, in which case we may disclose your personal data
            to the prospective seller or buyer of such business or assets; If
            Modellica Partners or substantially all of its assets are acquired
            by a third party, in which case personal data held by it about its
            customers will be one of the transferred assets; To law enforcement
            & government agencies, or authorized third parties, if we are under
            a duty to disclose or share your personal data in order to comply
            with any legal or regulatory obligation or request; To enforce our
            Terms and Conditions and other Agreements or to investigate
            potential breaches or for the purpose of publishing statistics
            relating to the use of the App, in which case all information may be
            aggregated or made anonymous. To report your Account to credit
            bureaus; Late payments, missed payments or, other defaults on your
            Account may be reflected in your credit report
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Where We store your personal data
          </Text>
          <Text style={{ marginBottom: 15 }}>
            We store the data that we collect from you in our secured servers.
            By submitting your personal data, you also agree to the collection,
            transfer, storing or processing of your personal data in the manner
            set out above. We will take all steps reasonably necessary to ensure
            that your data is treated, stored and processed securely and in
            accordance with this privacy Policy. Where we have given you (or
            where you have chosen) a password that enables you to access certain
            parts of the App, you are responsible for keeping this password
            confidential. We ask you not to share a password with anyone.
            Unfortunately, the transmission of information via the internet is
            not completely secure. Although we will do our best to protect your
            personal data, we cannot guarantee the security of your data
            transmitted to Our Service Sites; any transmission is at your own
            risk. Once we have received your information, we will use strict
            procedures and security features to try to prevent unauthorized
            access
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Data Security
          </Text>
          <Text style={{ marginBottom: 15 }}>
            Securing your data is of utmost priority to us. However, note that
            no method of transmission over the internet, or method of electronic
            storage is 100% secure. We implement and maintain appropriate
            safeguards to protect personal data, taking into account in
            particular the risks to you, presented by unauthorised or unlawful
            processing or accidental loss, destruction of, or damage to their
            personal data. While we strive to use commercially acceptable means
            to protect your personal data, we cannot guarantee its absolute
            security
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Your rights
          </Text>
          <Text style={{ marginBottom: 15 }}>
            We will use your data for the purposes of compiling statistics
            relating to our user base or loan portfolio and may disclose such
            information to any third party for such purposes, provided that such
            information will always be anonymous and is in line with the Terms
            and Conditions of use. Should we wish to use your personal
            information for marketing purposes, we will inform you prior to such
            use. You shall be entitled to prevent such usage by informing us,
            with 3 days of being contacted of the proposed use, that you do not
            wish to disclose such information in this manner. You can also
            exercise this right at any time by contacting us at email:………………..
            This does not, however, preclude us from using your information for
            our marketing purposes.
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Access
          </Text>
          <Text style={{ marginBottom: 15 }}>
            You have the right to stop our access to your personal data. Should
            you wish to stop sharing information, you can uninstall the Lica
            App.
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Links to Other Sites
          </Text>
          <Text style={{ marginBottom: 15 }}>
            This service may, from time to time, contain links to and from the
            websites of our partner networks, advertisers and affiliates. If you
            follow a link to any third party link, you will be directed to that
            site. However, please note that these websites are not operated by
            us and they have their own privacy policies. We strongly advise that
            you review the privacy of these websites. We have no control and we
            do not accept any responsibility or liability for their policies,
            contents or practices. Please check these policies before you submit
            any personal data to these websites.
          </Text>
          <Text style={{ marginBottom: 15, fontSize: 20, fontWeight: "bold" }}>
            Changes to Privacy Policy
          </Text>
          <Text style={{ marginBottom: 15 }}>
            We may update our privacy policy from time to time. Thus, you are
            advised to review this page periodically for any changes. We may
            notify you of any changes when you next start the App and by posting
            the new privacy policy on this page. The new terms may be displayed
            on-screen and you may be required to read and accept them to
            continue your use of the App. These changes are effective
            immediately after they are posted on this page and by continuing to
            use the App, you confirm your acceptance of this Policy together
            with such changes, and your consent to the terms set out therein.
          </Text>
          <Text style={{ marginBottom: 15 }}>
            If you have questions, comments, suggestions regarding this privacy
            policy, kindly contact us by email: info@getlica.com
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
