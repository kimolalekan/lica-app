import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  KeyboardAvoidingView,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";
import currencyFormatter from "currency-formatter";

export default class NewLoan extends React.Component {
  state = {
    amount: ""
  };

  static navigationOptions = {
    title: "Please wait...",
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" }
  };

  formatMoney(val) {
    val = currencyFormatter.format(val, {
      symbol: "₦",
      decimal: ".",
      thousand: ",",
      precision: 0,
      format: "%s %v"
    });
    return val;
  }

  handleAmount(amount) {
    amount = this.formatMoney(amount);
    this.setState({
      amount: amount
    });
  }

  handleProc() {
    const { navigate } = this.props.navigation;

    if (this.state.amount) {
      navigate("Prequalified");
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />

        <View
          style={{
            alignItems: "center",
            marginTop: 50
          }}
        >
          <TextInput
            placeholder="Amount"
            keyboardType="number-pad"
            maxLength={11}
            style={{
              backgroundColor: "transparent",
              padding: 10,
              margin: 5,
              borderColor: "#ccc",
              borderWidth: 1,
              borderRadius: 5,
              minHeight: 40,
              width: 200,
              fontSize: 20
            }}
            value={this.state.amount}
            onChangeText={val => this.handleAmount(val)}
          />

          <TouchableOpacity
            onPress={() => this.handleProc()}
            style={{
              backgroundColor: "#003B55",
              padding: 10,
              margin: 5,
              borderRadius: 5,
              width: 200,
              fontSize: 20
            }}
          >
            <Row>
              <Col sm={9}>
                <Text
                  style={{
                    color: "#fff",

                    fontSize: 16
                  }}
                >
                  Request Loan
                </Text>
              </Col>
              <Col sm={3}>
                <Icon name="ios-arrow-round-forward" color="#fff" size={23} />
              </Col>
            </Row>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
