import React from "react";
import { StyleSheet, Text, View, ScrollView, StatusBar } from "react-native";

export default class Faq extends React.Component {
  static navigationOptions = {
    title: "Terms and Conditions",
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" }
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <StatusBar barStyle="light-content" />

        <View style={{ margin: 10 }}>
          <Text>Terms and conditions text here.</Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
