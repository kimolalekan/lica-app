import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo";
import AppIntroSlider from "react-native-app-intro-slider";

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  image: {
    width: "100%",
    height: Dimensions.get("window").height >= 667 ? 300 : 250,
    resizeMode: "contain"
  },
  text: {
    color: "rgba(255, 255, 255, 0.8)",
    backgroundColor: "transparent",
    textAlign: "center",
    paddingHorizontal: 16
  },
  title: {
    fontSize: 22,
    color: "white",
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 16
  }
});

const slides = [
  {
    key: "somethun",
    title: "Get access to loan in minutes.",
    text: "",
    image: require("../images/loan-hazard.png"),
    colors: ["#30cfd0", "#330867"]
  },
  {
    key: "somethun1",
    title: "No unnecessary documents",
    text: "",
    image: require("../images/doc-hazard.png"),
    colors: ["#fccb90", "#d57eeb"]
  }
];

export default class Intro extends React.Component {
  static navigationOptions = {
    title: null,
    header: null
  };

  _renderItem = props => (
    <LinearGradient
      style={[
        styles.mainContent,
        {
          width: props.width,
          height: props.height
        }
      ]}
      colors={props.colors}
      start={{ x: 0, y: 0.1 }}
      end={{ x: 0.1, y: 1 }}
    >
      <Image source={props.image} style={styles.image} />
      <View>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.text}>{props.text}</Text>
      </View>
    </LinearGradient>
  );

  async onDone() {
    const { navigate } = this.props.navigation;
    await AsyncStorage.setItem("welcomed", "true")
      .then(() => {
        navigate("Landing");
      })
      .catch(err => console.log(err));
  }

  render() {
    return (
      <AppIntroSlider
        slides={slides}
        renderItem={this._renderItem}
        bottomButton
        doneLabel="Get started"
        onDone={this.onDone.bind(this)}
      />
    );
  }
}
