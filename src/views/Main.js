import React, { Component } from "react";
import { createBottomTabNavigator } from "react-navigation";
import { Text, Image, TouchableOpacity } from "react-native";

import Loans from "./Loans";
import Profile from "./Profile";

const tabList = [
  {
    text: " Loans",
    redirectLink: "Loans",
    id: "Loans"
  },
  {
    text: " Profile ",
    redirectLink: "Profile",
    id: "Profile"
  }
];

const renderRightMenu = (index, navigation) => {
  let menu = null;
  if (index == 1) {
    menu = (
      <TouchableOpacity
        style={{ padding: 5 }}
        onPress={() => {
          navigation.navigate("UpdateProfile");
        }}
      >
        <Text style={{ color: "#fff", marginRight: 7 }}>Edit</Text>
      </TouchableOpacity>
    );
  } else {
    menu = (
      <TouchableOpacity
        style={{ padding: 5 }}
        onPress={() => {
          navigation.navigate("NewLoan");
        }}
      >
        <Text style={{ color: "#fff", marginRight: 7 }}>Request Loan</Text>
      </TouchableOpacity>
    );
  }

  return menu;
};

const tabBarConfiguration = {
  tabBarOptions: {
    style: {
      height: 50,
      backgroundColor: "white",
      borderTopWidth: 1,
      borderTopColor: "#ccc",
      elevation: 1
    },
    showLabel: true,
    showIcon: true
  },
  tabBarPosition: "bottom",
  animationEnabled: true,
  swipeEnabled: false
};

const Main = createBottomTabNavigator(
  {
    Loans: {
      screen: Loans,
      navigationOptions: {
        tabBarLabel: ({ focused }) => (
          <Text
            style={{
              marginBottom: 5,
              textAlign: "center",
              color: focused ? "#D43790" : "#ccc"
            }}
          >
            {tabList[0].text}
          </Text>
        ),
        tabBarIcon: ({ focused }) =>
          focused ? (
            <Image
              source={require("../images/icons/activity-fill.png")}
              style={{
                width: 20,
                height: 20,
                resizeMode: "contain",
                marginTop: 25,
                marginBottom: 25
              }}
            />
          ) : (
            <Image
              source={require("../images/icons/activity.png")}
              style={{
                width: 20,
                height: 20,
                resizeMode: "contain",
                marginTop: 25,
                marginBottom: 25
              }}
            />
          )
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarLabel: ({ focused }) => (
          <Text
            style={{
              marginBottom: 5,
              textAlign: "center",
              color: focused ? "#D43790" : "#ccc"
            }}
          >
            {tabList[1].text}
          </Text>
        ),
        tabBarIcon: ({ focused }) =>
          focused ? (
            <Image
              source={require("../images/icons/person-fill.png")}
              style={{
                width: 20,
                height: 20,
                resizeMode: "contain",
                marginTop: 25,
                marginBottom: 25
              }}
            />
          ) : (
            <Image
              source={require("../images/icons/person.png")}
              style={{
                width: 20,
                height: 20,
                resizeMode: "contain",
                marginTop: 25,
                marginBottom: 25
              }}
            />
          )
      }
    }
  },
  tabBarConfiguration
);

Main.navigationOptions = ({ navigation }) => {
  const { routeName } = navigation.state.routes[navigation.state.index];
  const headerTitle = routeName;

  return {
    headerTitle,
    headerStyle: {
      backgroundColor: "#cb0000"
    },
    headerTintColor: "#fff",
    headerLeft: null,
    headerRight: renderRightMenu(navigation.state.index, navigation)
  };
};

export default Main;
