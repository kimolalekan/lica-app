import React, { Component } from "react";
import { Text, View, StyleSheet, WebView } from "react-native";
import { Constants } from "expo";

export default class Paystack extends Component {
  static navigationOptions = {
    title: "Standing Order",
    headerTintColor: "#fff",
    headerStyle: { backgroundColor: "#cb0000" }
  };

  onNavigationStateChange = navState => {
    if (navState.url.indexOf("https://getlica.com/") === 0) {
      console.log("Done");
    }
  };

  render() {
    const url = "https://checkout.paystack.com/gv3wi41frf9qgxu";
    return (
      <WebView
        source={{
          uri: url
        }}
        onNavigationStateChange={this.onNavigationStateChange}
        startInLoadingState
        scalesPageToFit
        javaScriptEnabled
        style={{ flex: 1 }}
      />
    );
  }
}
