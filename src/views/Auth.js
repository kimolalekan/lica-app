import React from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  ImageBackground,
  AsyncStorage,
  ActivityIndicator
} from "react-native";

export default class Auth extends React.Component {
  static navigationOptions = {
    title: null,
    header: null
  };

  authenticate = async () => {
    const { navigate } = this.props.navigation;
    let user = await AsyncStorage.getItem("user");
    user ? navigate("Main") : navigate("Landing");
  };

  componentDidMount() {
    setTimeout(() => {
      this.authenticate();
    }, 3000);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />

        <ImageBackground
          style={styles.bg}
          source={require("../images/lica-bg.png")}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: "40%",
              bottom: 0
            }}
          >
            <View style={styles.loader}>
              <ActivityIndicator size={"large"} color="#003B55" />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 50
  }
});
