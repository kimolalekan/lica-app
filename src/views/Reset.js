import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  KeyboardAvoidingView,
  AsyncStorage,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { Column as Col, Row } from "react-native-flexbox-grid";

export default class Reset extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      verify: "",
      error: "",
      notify: "none"
    };
  }
  static navigationOptions = {
    title: null,
    header: null
  };

  validate = async () => {
    let verify = this.state.verify;
    let code = await AsyncStorage.getItem("code");
    if (code === verify) {
      const { navigate } = this.props.navigation;
      navigate("Renew");
    } else {
      this.setState({ error: "Invalid code!!!" });
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar barStyle="light-content" />

        <ImageBackground
          style={styles.bg}
          source={require("../images/lica-bg.png")}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: "35%",
              bottom: 0
            }}
          >
            <View style={{ alignItems: "center" }}>
              <Text style={{ marginBottom: 10 }}>
                Reset code has been sent to your phone.
              </Text>
              <Text style={{ color: "#cb0000", marginBottom: 20 }}>
                {this.state.error}
              </Text>
              <TextInput
                placeholder="Enter code"
                keyboardType="phone-pad"
                maxLength={6}
                style={{
                  backgroundColor: "transparent",
                  padding: 7,
                  margin: 5,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5,
                  height: 50,
                  width: 250,
                  fontSize: 20
                }}
                onChangeText={verify => this.setState({ verify })}
              />
              <TouchableOpacity
                onPress={() => this.validate()}
                style={{
                  backgroundColor: "#cb0000",
                  padding: 8,
                  margin: 5,
                  borderRadius: 5,
                  width: 250
                }}
              >
                <Row>
                  <Col sm={10}>
                    <Text
                      style={{
                        color: "#fff",
                        fontSize: 18
                      }}
                    >
                      Verify code
                    </Text>
                  </Col>
                  <Col sm={2}>
                    <Icon
                      name="ios-arrow-round-forward"
                      color="#fff"
                      size={30}
                    />
                  </Col>
                </Row>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => navigate("Forget")}>
                <Text
                  style={{
                    color: "#000",
                    fontSize: 14,
                    marginTop: 15
                  }}
                >
                  Didn't receive SMS code? Click here to try again.{" "}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
