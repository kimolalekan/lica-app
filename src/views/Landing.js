import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  KeyboardAvoidingView
} from "react-native";

import { Permissions } from "expo";

export default class Landing extends React.Component {
  static navigationOptions = {
    title: null,
    header: null
  };

  checkMultiPermissions = async () => {
    const { status, expires, permissions } = await Permissions.askAsync(
      Permissions.LOCATION,
      Permissions.CONTACTS,
      Permissions.NOTIFICATIONS
    );
    if (status !== "granted") {
      alert("Hey! You heve not enabled selected permissions");
    }
  };

  componentDidMount() {
    this.checkMultiPermissions().catch(err => console.log(err));
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView style={styles.container} enabled>
        <StatusBar barStyle="light-content" />

        <ImageBackground
          style={styles.bg}
          source={require("../images/lica-bg.png")}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: "40%",
              bottom: 0
            }}
          >
            <View style={{ alignItems: "center" }}>
              <TouchableOpacity
                onPress={() => navigate("SmsVerify")}
                style={{
                  backgroundColor: "#cb0000",
                  paddingTop: 10,
                  paddingBottom: 10,
                  margin: 5,
                  borderRadius: 5,
                  width: 200
                }}
              >
                <Text
                  style={{
                    color: "#fff",
                    textAlign: "center",
                    fontSize: 20
                  }}
                >
                  Signup
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigate("Login")}
                style={{
                  backgroundColor: "#003B55",
                  paddingTop: 10,
                  paddingBottom: 10,
                  margin: 5,
                  borderRadius: 5,
                  width: 200
                }}
              >
                <Text
                  style={{
                    color: "#fff",
                    textAlign: "center",
                    fontSize: 20
                  }}
                >
                  Login
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    flex: 1,
    alignItems: "center"
  },
  header: {
    fontSize: 50,
    fontWeight: "bold",
    marginTop: 100,
    color: "#fff",
    textAlign: "center"
  },
  text: {
    fontSize: 12,
    color: "#fff",
    marginTop: 20
  },
  loader: {
    marginTop: 100
  }
});
