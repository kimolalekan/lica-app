import React from "React";
import { Text, View } from "react-native";
const Toc = () => (
  <View style={{ margin: 10 }}>
    <Text style={{ fontSize: 18, fontWeight: "bold", marginBottom: 15 }}>
      Terms and Conditions
    </Text>
    <Text style={{ marginBottom: 15 }}>
      The following Terms and Conditions apply to and regulate the provision of
      products and services advanced by Modellica Partners Ltd (hereinafter
      called "the Lender" or " Modellica " or “We”) to the Borrower herein.
      These Terms and Conditions constitute the Lender's offer and sets out the
      terms governing this Agreement.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Lica is credit product offered by the Lender and BY ACCEPTING THIS ONLINE
      OFFER, A LICA LOAN ACCOUNT IS SET UP WITH MODELLICA and you agree that you
      have read these Terms and Conditions. You authorize the Lender to review
      your credit report and you understand that this Account may be subject to
      transaction fees and default fees and is governed by the Laws of the
      Federal Republic of Nigeria.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You will be asked to provide information (such as name, phone number, your
      date of birth, account details and your Bank Verification Number) when you
      apply for a Lica loan. Please read these Terms and Conditions carefully
      before using the Lica App. By accessing or using the Lica App, you agree
      to be bound by the Terms and Conditions stated herein. These Terms and
      Conditions are subject to change and any changes will be incorporated into
      these Terms and Conditions from time to time.{" "}
    </Text>
    <Text style={{ marginBottom: 15 }}>
      If you do not wish to be bound by this Agreement, do not access or use the
      Lica App. Kindly note that all our products and services may be subject to
      additional separate terms and conditions which govern their use.{" "}
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      A. DEFINITIONS
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Terms such as: "You", "Your", “Customer”, “Borrower” are references to the
      person accessing or using the Lica App, while "We", "us" or "our" and
      "Lender" are references to Modellica, an entity duly incorporated and
      licensed by the laws and regulations of the Federal Republic of Nigeria to
      provision to provide this service either directly or in partnership.{" "}
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; "Account" means the Customer's account with the Lender
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; “Agreement” means the legal agreement created between you and us
      that includes all terms, definitions, and provisions set forth in these
      Terms and Conditions
    </Text>
    <Text style={{ marginBottom: 15 }}>
      “App” means a Lica mobile application used to access the Lica service
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; "Disbursement Date" means the date the Lender actually advanced
      the loan to the Borrower
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; "Payment Due Date" means a maximum of 30 days after the loan has
      been given
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; "Credit Limit" means the maximum credit available to the Customer
      on opening the account with the Lender
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; "Loan" means the amount advanced to the Customer by the Lender,
      which shall be determined based on the information provided by the
      provider
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; “Default” means Customer’s failure to meet financial obligations
      due to the Lender, as and when due
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &#11044; “Personal Data” means any data, details, or information about you
      or your activities, provided to us either directly by you or by a third
      party, or gathered through the access, view, or use of the Lica mobile
      application or any other service provided by us. This includes technical
      information about your Devices and related software, hardware and
      peripherals you use, your call data records, phone contacts, SMS logs, and
      any other information about you we deem appropriate to collect from time
      to time to offer you services
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      B. ACCEPTANCE OF AGREEMENT
    </Text>
    <Text style={{ marginBottom: 15 }}>
      BY downloading, opening an Account and using the Lica App, you indicate
      that you unconditionally accept the terms of this Agreement and you agree
      to abide by them
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      C. TERM OF AGREEMENT
    </Text>
    <Text style={{ marginBottom: 15 }}>
      This Agreement is in effect until you discontinue your use of the Lica APP
      and all financial obligations with regard to your use of the App have been
      fulfilled. ​
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      D. RIGHT TO MODIFY AGREEMENT
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We reserve the right to modify these terms and conditions at any time
      without notice to you by posting to this website. Your continued use of
      the App will constitute your acceptance of any such changes.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      E. OWNERSHIP OF SERVICE
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We are and remain the owner of the App and the services available through
      it at all times, and you must not attempt to rent, lease, sub-license,
      copy, transmit, distribute, reproduce (for compensation or otherwise),
      license, alter, adapt or modify the whole or any part of the lica App in
      any way.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      F. RIGHT TO MODIFY SERVICE
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We reserve the right to modify the App, in any way we deem appropriate or
      necessary, without notice to you. Your continued Use of the Lica App will
      constitute your acceptance of any such changes.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      G. LICENSE TO USE THE SERVICE
    </Text>
    <Text style={{ marginBottom: 15 }}>
       You must not use the Lica App and any of the services available through
      it in any unlawful manner, for any unlawful purpose, or in any manner
      inconsistent with this Agreement, or act fraudulently or maliciously, for
      example, by hacking into or inserting malicious code, including viruses,
      or harmful data, into the App, any service or any operating system.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      The services offered on Lica App can only be utilized by persons over the
      age of 18. Modellica reserves the right to verify the authenticity and
      status of your bank accounts and Bank Verification Numbers with the
      relevant Payment System Provider. Our acceptance of your application for
      an Account does not create any contractual relationship between you and
      the Payment System Provider. We reserve the right to decline your
      application for a Loan or to revoke the same at any stage at our sole and
      absolute discretion and without assigning any reason or giving any notice
      thereto.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We reserve the right to issue, decline to issue a Loan depending on the
      assessment of the credit profile of each individual borrower from time to
      time. The terms of the Loan and the amount payable in relation to each
      loan application will be displayed on the App.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      H. SERVICE AVAILABILITY
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Use of the App may from time to time be unavailable, delayed, limited or
      slow due to, but not restricted to the following factors:
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Force majeure{"\n"} Hardware failure {"\n"} Software failure{"\n"}
      Overload of system capacities {"\n"}Government or regulatory restrictions,
      court or tribunal rulings, amendment of legislation or other human
      intervention{"\n"} Any other cause whatsoever beyond our control
    </Text>
    <Text style={{ marginBottom: 15 }}>
       You acknowledge and agree that internet transmission is never completely
      private or secure. You understand that any message or information you send
      using the Lica Service may be read or intercepted by others, even if there
      is a special notice that a particular transmission is encrypted.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      All content and services on or available through the Services are provided
      on an "as is" basis and we do not make any representation or give any
      warranty or guarantee in respect of the Lica Service or its content.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We may discontinue or make changes in the Lica Service at any time without
      prior notice to you and without any liability to you.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We may in our sole discretion terminate your Use of the Lica Service for
      any reason, including without limitation where we believe that you have
      not acted in accordance with the terms and conditions of this Agreement.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      The information provided by the Service is believed to be reliable, but we
      do not warrant its completeness, timeliness or accuracy.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      I. AUTHORISATION TO COMPLY
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree that we are irrevocably authorised to comply with any
      instructions received on your behalf through the App and it is agreed that
      such instruction shall be irrevocably deemed to be your instruction
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      J. RIGHT TO USE YOUR PERSONAL DATA
    </Text>

    <Text style={{ marginBottom: 15 }}>
      By using the App, You hereby agree and authorise Modellica to verify
      information provided by you against the information held by the Payment
      System Providers pursuant to the Agreement between you and the relevant
      Payment System Provider for the provision of its products and services.
      You hereby agree and authorise the Lender to verify information including,
      but not limited to, data relating to your phone (including, without
      limitation, your phone’s history) from your Device, from any SMS sent to
      you or by you, from any 3rd party applications, your phone number, name,
      date of birth, account details, information from the Credit Bureaus and
      you further agree and consent to the disclosure and provision of such
      Personal Information by the Credit Bureaus and such other information as
      Lender shall require for purposes of identifying you and providing you the
      Services (the “Relevant Information”).
    </Text>

    <Text style={{ marginBottom: 15 }}>
      We agree to only use your Personal Data for the express purpose of
      providing services to you directly or through a third party, ascertaining
      the risk of providing services to you, or communicating with you about
      services you are using, or communicating with you about services you are
      or may be interested in, or otherwise as described by this Agreement.{" "}
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You acknowledge and agree that information on your non-compliance with the
      Terms and Conditions of this Agreement may be transferred to the Credit
      Bureaus.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      K. RIGHT TO ENFORCE COLLECTION
    </Text>
    <Text style={{ marginBottom: 15 }}>
      By using the Lica App, you agree to grant us an irrevocable written
      authorisation to issue open-ended direct debit mandates for all accounts
      you own now, or at any point in the future. If you default, you agree that
      we have your written authorisation to place a lien on every account you
      own now, or at any point in the future, to recover any outstanding balance
      you owe us by the execution of the direct debit mandate. You further agree
      that If you default we may execute such direct debit mandates, singularly
      or severally, from one or more of your accounts, without any notice or
      further obligation to you, and may continue to execute direct debit
      mandates on your accounts until such time as your financial obligations to
      us have been fulfilled. The Lender warrant and represent that we will only
      execute a direct debit mandate if you Default
    </Text>
    <Text style={{ marginBottom: 15 }}>
      ​If you default, you agree that we shall have the right to assign an
      external Collections Agency who will take all reasonable steps to collect
      the outstanding loan amount. We have the right to institute legal
      proceedings against the defaulting Borrower and We are under no obligation
      to inform the Borrower before such proceedings commence. Also, the
      defaulting Borrower shall be responsible for all legal costs and expenses
      incurred by the Lender in attempting to obtain repayment of any
      outstanding loan balance owed by the Borrower. Interest on any amount
      which becomes due and payable shall be charged{" "}
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      L. RIGHT TO NOTIFY
    </Text>
    <Text style={{ marginBottom: 15 }}>
      By using the Lica App, you grant the Lender an irrevocable written
      authorisation, to notify anyone, publicly or privately, by any means of
      communication We deem appropriate or necessary, about your use of the App.
      You agree to allow us to use your personal data, or any other means or
      manner We deem appropriate or necessary, to determine whom to notify. We
      warrant and represent that We will only make such notifications as it
      pertains to your use of the App.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      M. BORROWER’S OBLIGATIONS
    </Text>

    <Text style={{ marginBottom: 15 }}>
      You agree to pay us any financial obligations that result from the use of
      the Lica App by the due date agreed when the service was extended to you.
      These financial obligations, whether such obligations are loan sum,
      interest or fees, penalties, or any other financial obligations
      communicated to you from time to time. You acknowledge that failure to
      fulfill such financial obligations by the agreed due date for those
      obligations constitutes a breach of this Agreement
    </Text>
    <Text style={{ marginBottom: 15 }}>
      The Lender reserves the right to presume that the you have authorized any
      loan application made on your behalf through the Lica App. You will be
      deemed responsible for any unauthorized application using this App unless
      you notify the Lender, in writing, of imminent fraud by another person on
      your Account within 24 hours of such fraud
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to use of the Lica App for your personal purposes only
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to provide accurate and current information that may be
      requested from you from time to time
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to pay penalty fee that may be charged to your Account in the
      event of late repayment or default
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to make all payments via direct debit on cards or by electronic
      funds transfer as requested
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree not to access or use the Lica App unless you are of legal age
      and have the capacity to enter into a valid contract
    </Text>

    <Text style={{ marginBottom: 15 }}>
      You agree not to provide information that does not belong to you to access
      or use the Lica App
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to change your card details and/or account details immediately
      you are made aware that it has become known to someone else. You
      acknowledge that failure to do so immediately is considered a breach of
      this Agreement
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to urgently notify us immediately upon your suspicion that the
      confidentiality of your personal data (including card and account details)
      have been compromised
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You will not use the Lica App in any unlawful manner, or for any unlawful
      purpose, or act fraudulently or maliciously, for example, by hacking into
      or inserting malicious code, including viruses, or harmful data, into the
      App{" "}
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You will not to disassemble, decompile, reverse-engineer or create
      derivative works based on the whole or any part of the App or attempt to
      do any such thing except to the extent that such actions cannot be
      prohibited because they are essential for the purpose of achieving
      inter-operability of the App with another software program
    </Text>
    <Text style={{ marginBottom: 15 }}>
       You will comply with any applicable law and regulation relating to use of
      the Lica App, including any applicable law or regulation governing the
      downloading of any App, otherwise exporting the technology used or
      supported by the App, which may include but is not limited to not using
      the App in a country where the use of such Apps is unlawful;  You will not
      use the App in any way other than for the purposes expressly described in
      this Agreement; and
    </Text>
    <Text style={{ marginBottom: 15 }}>
       You will not attempt to gain unauthorized entry into the Lender’s systems
      or any Third Party systems, or misuse of any information provided by the
      Service.
    </Text>

    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      N. INDEMNIFICATION
    </Text>
    <Text style={{ marginBottom: 15 }}>
      With respect to Modellica complying with your instructions or requests in
      relation to your Account, you undertake to indemnify Modellica and hold it
      harmless against any loss, charge, damage, expense, fee or claim which
      Modellica suffers or incurs or sustains thereby and you absolve Modellica
      from all liability for loss or damage which you may sustain from Modellica
      acting on your instructions or requests or in accordance with these Terms
      and Conditions
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to indemnify Modellica for the Lica App not meeting your
      individual requirements or the Lica App containing defects or errors, as
      the App has not been developed specifically for you. You acknowledge it is
      your responsibility to ensure that the facilities and functions of the App
      meet your requirements
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to indemnify Modellica from any loss or damage caused by a
      distributed denial-of-service attack, viruses or other technologically
      harmful material that may infect your Device, data or other proprietary
      material due to your use of the Lica App.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Modellica’s maximum liability to you in respect of any instruction you
      make using the Lica App shall be to refund the interest or fees paid to us
      for a particular financial transaction, and/or to refund any proven
      erroneous payment above any interest or fees due to us for a particular
      loan transaction
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We are exempted from any form of liability whatsoever for complying with
      any or all instruction(s) given by means of your personal data provided
      and accordingly, we shall not be responsible for any fraudulent, duplicate
      or erroneous instructions
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We are expressly exempted from any liability arising from unauthorized
      access to your Accounts and/or data as supplied on our platform, and/or
      any breach of security or any destruction or accessing of your data or any
      destruction or theft of or damage to any of your Device
    </Text>
    <Text style={{ marginBottom: 15 }}>
      We are exempted from any liability as regards breach of duty of secrecy
      arising out of your inability to observe and maintain the secrecy of your
      personal data We are exempted from any liability for loss that may arise
      as a result of acts of hackers and other unauthorized access to your
      Account via the Lica App
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree to indemnify us from any loss or damage occasioned by your
      failure to adhere to these Terms and Conditions and/or by supplying
      incorrect information or loss or damage occasioned by the failure or
      unavailability of third party facilities or systems or the inability of a
      third party to process a transaction or any loss which may be incurred by
      Modellica as a consequence of any breach of these Terms and Conditions
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Nothing herein shall be interpreted as limiting or reducing our
      obligations to customers in accordance with applicable laws in force in
      the Federal Republic of Nigeria and banking and other financial
      institutions regulations that may be laid down from time to time
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      O. TERMINATION
    </Text>
    <Text style={{ marginBottom: 15 }}>
      This Agreement may be terminated immediately with or without notice, if
      you have breached this Agreement at any time.   Modellica reserve the
      right to cease operating the Lica App and terminate this Agreement at any
      time without notice.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      P. NON-WAIVER
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Failure or omission by the Borrower or Lender at any time to enforce or
      require strict or timely compliance with any provision of this Agreement
      shall not affect or impair that provision in any way or the rights of that
      party to avail itself of the remedies it may have in respect of any breach
      of that provision.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      Q. SEVERABILITY
    </Text>
    <Text style={{ marginBottom: 15 }}>
      If any part of this Agreement is held by any court or administrative body
      of competent jurisdiction to be illegal, void or unenforceable, such
      determination shall not impair the enforceability of the remaining parts
      of this Agreement.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      R. ENFORCEABILITY
    </Text>
    <Text style={{ marginBottom: 15 }}>
      If for any reason a court of competent jurisdiction or a Regulator finds
      any provision of this Agreement, or portion thereof, to be unenforceable,
      we will replace that provision or portion of the Agreement with such
      enforceable provision or portion as comes closest to the intent underlying
      the unenforceable term or condition.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      S. GOVERNING LAW
    </Text>
    <Text style={{ marginBottom: 15 }}>
      This Agreement to access and use Lica App shall be governed by, and
      interpreted in accordance with the laws of the Federal Republic of
      Nigeria.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      T. DISPUTE RESOLUTION
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree that all disputes between you and us (whether or not such
      dispute involves a third party) with regard to your relationship with us,
      including without limitation, disputes related to this Agreement, your use
      of the Lica App, and/or rights of privacy and/or publicity, will be
      resolved by binding arbitration
    </Text>
    <Text style={{ marginBottom: 15 }}>
      You agree that you may only bring claims on your own behalf. Neither you
      nor we will participate in a class action or class-wide arbitration for
      any claims covered by this Agreement to arbitrate. You are giving up your
      right to participate as a class representative or class member on any
      class claim you may have against us including any right to class
      arbitration or any consolidation of individual arbitrations. You also
      agree not to participate in claims brought in a private solicitation or
      representative capacity, or consolidated claims involving another person's
      account, if we are a party to the proceeding
    </Text>
    <Text style={{ marginBottom: 15 }}>
      In the case of such a dispute, you agree to present to us, in writing, a
      letter specifying the nature of your dispute, and the Parties (you and us)
      shall use our good faith efforts to resolve any dispute, controversy or
      claim of any nature whatsoever arising out of or in relation to or in
      connection with this Agreement. In the event that an amicable settlement
      has not been reached within thirty (30) days of the Parties’
      representatives meeting as aforesaid, you must notify us in writing the
      point(s) in issue and your intention to refer the dispute to mediation at
      the Lagos Multi-Door Court House. If the dispute is not resolved by
      mediation at the Lagos Multi-Door Court within a period of 14 days from
      the date of submission, we both agree that we choose to refer the dispute
      to arbitration in accordance with the Arbitration and Conciliation Act Cap
      A18, Laws of the Federation of Nigeria 2004. The arbitration shall be by a
      sole arbitrator appointed jointly by the Parties. If the Parties do not
      agree on a person to act as the sole arbitrator, the Chief Judge of the
      Federal High Court shall appoint the sole arbitrator. The place of
      arbitration shall be Lagos, Nigeria. The language of Arbitration shall be
      English.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      This arbitration clause will survive the termination of this Agreement.
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 18, fontWeight: "bold" }}>
      U. MODELLICA’S PRIVACY POLICY
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Our use of your personal information will only be in accordance with our
      Privacy Policy. Please take the time to read our Privacy Policy, as it
      includes important terms which apply to you.
    </Text>
    <Text style={{ marginBottom: 15 }}>
      Upon downloading and signing up on Lica App, this signify your acceptance
      of these Terms and Conditions and an intention to use the Lica App. You
      will be deemed to have accepted Lica’s Privacy Policy, a copy of which is
      available on the App.
    </Text>
  </View>
);

export default Toc;
