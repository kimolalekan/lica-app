import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";

import Auth from "../views/Auth";
import Intro from "../views/Intro";
import Loans from "../views/Loans";
import Loan from "../views/Loan";
import LoanOption from "../views/LoanOption";
import NewLoan from "../views/NewLoan";
import Signup from "../views/Signup";
import Login from "../views/Login";
import Forget from "../views/Forget";
import Reset from "../views/Reset";
import Renew from "../views/Renew";
import Landing from "../views/Landing";
import Main from "../views/Main";
import Profile from "../views/Profile";
import UpdateProfile from "../views/UpdateProfile";
import UpdateFinance from "../views/UpdateFinance";
import Verify from "../views/Verify";
import SmsVerify from "../views/SmsVerify";
import Privacy from "../views/Privacy";
import Terms from "../views/Terms";
import Faq from "../views/Faq";
import Paystack from "../views/Paystack";
import Prequalified from "../views/Prequalified";

const Routes = createStackNavigator({
  Auth: { screen: Auth },
  Intro: { screen: Intro },
  Landing: { screen: Landing },
  Main: { screen: Main },
  SmsVerify: { screen: SmsVerify },
  Verify: { screen: Verify },
  Loans: { screen: Loans },
  Loan: { screen: Loan },
  LoanOption: { screen: LoanOption },
  NewLoan: { screen: NewLoan },
  LoanOption: { screen: LoanOption },
  Prequalified: { screen: Prequalified },
  Paystack: { screen: Paystack },
  Signup: { screen: Signup },
  Login: { screen: Login },
  Forget: { screen: Forget },
  Reset: { screen: Reset },
  Renew: { screen: Renew },
  Profile: { screen: Profile },
  UpdateProfile: { screen: UpdateProfile },
  UpdateFinance: { screen: UpdateFinance },
  Privacy: { screen: Privacy },
  Terms: { screen: Terms },
  Faq: { screen: Faq }
});

export default createAppContainer(Routes);
