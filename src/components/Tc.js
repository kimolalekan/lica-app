import React from "React";
import { Text, View, Linking } from "react-native";

const openLink = () => {
  Linking.openURL("https://expo.io");
};

const Tc = () => (
  <View style={{ margin: 10 }}>
    <Text style={{ fontSize: 20, fontWeight: "bold", marginBottom: 15 }}>
      Terms and Conditions
    </Text>
    <Text style={{ marginBottom: 15, fontSize: 16, fontWeight: "bold" }}>
      I hereby consent;
    </Text>
    <Text style={{ marginBottom: 15 }}>
      &middot; To permit Lica to debit your account of the due amount on due
      date
    </Text>
    <Text style={{ marginBottom: 15 }}>
      To pay the loan balance on or before the due date
    </Text>
    <Text style={{ marginBottom: 15 }}>
      To allow use of your personal data and other information provided to
      determine loan offers
    </Text>
    <Text style={{ marginBottom: 15 }}>
      To be subject to fees and penalties for late payment
    </Text>
    <Text style={{ marginBottom: 15 }}>
      To allow Lica reach out to your contacts if you do default on due payment
    </Text>
    <Text style={{ marginBottom: 15 }}>
      To permit Lica to report you to the Credit Bureaus if you delay or default
      in payment. Note that this will negatively impact your credit score and
      ability to take loan from any source in Nigeria
    </Text>
    <Text style={{ marginBottom: 15 }} onPress={openLink}>
      For further information on Terms and Conditions, please refer to the
      website, https://gelica.com/terms.html
    </Text>
  </View>
);

export default Tc;
