/*

Styles
 */
import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  conversationContainer: {
    maxWidth: 250,
    backgroundColor: "white",
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 0,
    borderTopLeftRadius: 10,
    padding: 10,
    marginTop: 10
  },
  convLeft: {
    alignSelf: "flex-start"
  },
  convRight: {
    alignSelf: "flex-end",
    backgroundColor: "#BE0071",
    paddingTop: 5,
    borderBottomRightRadius: 0,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderTopLeftRadius: 10
  },
  convRightText: {
    color: "white",
    alignSelf: "center"
  },
  conversationText: {
    lineHeight: 20,
    alignSelf: "flex-start"
  },
  fullWidthImage: {
    width: 230,
    height: 200,
    resizeMode: "contain"
  }
});
